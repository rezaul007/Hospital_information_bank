<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<style type="text/css">
#message1{font-size:24px;color:green;}
#message2{font-size:24px;color:red;}

</style>
<body>
	<?php
    	foreach($model as $d)
		{
	?>		
        
		<form action="<?php echo base_url();?>Addhospital/update"  method="post">
        	<input type="hidden" name="id" value="<?php echo $d->hosp_id;?>" />
			<fieldset>
			<h2 class="sub-title">Edit Form</h2>
			<span class="input-group-addon"><span class="input-text-style">&nbsp;&nbsp;&nbsp;Hospital Name</span></span>
			<input type="text" name="nm" required class="form-control input-lg" placeholder="Hospital Name" value="<?php echo $d->hosp_name;?>">
			
                        <span class="input-group-addon"><span class="input-text-style">&nbsp;&nbsp;&nbsp;Hospital Address</span></span>
			<input type="text" name="hosp_add" required class="form-control input-lg" placeholder="Hospital Address" value="<?php echo $d->hosp_add;?>">
			
                        <span class="input-group-addon"><span class="input-text-style">&nbsp;&nbsp;&nbsp;Hospital Contact</span></span>
			<input type="text" name="hosp_con" required class="form-control input-lg" placeholder="Hospital Contact" value="<?php echo $d->hosp_con;?>">
			
                        <span class="input-group-addon"><span class="input-text-style">&nbsp;&nbsp;&nbsp;Hospital E-mail</span></span>
			<input type="email" name="email"  placeholder="hospital Name" value="<?php echo $d->Email;?>">
			
                        <span class="input-group-addon"><span class="input-text-style">&nbsp;&nbsp;&nbsp;Hospital Web Address (If Available)</span></span>
			<input type="text" name="web"  placeholder="hospital Web Address" value="<?php echo $d->Web;?>">
			
			<span class="input-group-addon"><span class="input-text-style">&nbsp;&nbsp;&nbsp;Area Name</span></span>
			<select name="cid" class="form-control input-lg">
            	<?php
                       foreach ($all_area as $c) 
					   {
                              echo "<option value=\"{$c->area_id}\">{$c->area_name}</option>";
                             
                       }
                 ?>
            </select>
                        
            	
			</fieldset>
			<input type="submit"  value="Update" class="btn btn-custom-2 btn-lg md-margin">
		</form>
        
     <?php }?>                             
</body>
</html>