<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mymodel_model extends CI_Model {
	public function view_data_two_table($name)
	{
		$this->db->where("area.area_name",$name);
		$this->db->select("hospitals.hosp_id, hospitals.hosp_name,hospitals.hosp_add,hospitals.hosp_con,hospitals.Email,hospitals.area_id,hospitals.Web");
		$this->db->from("hospitals");
		$this->db->join("area","hospitals.area_id=area.area_id");
		return $this->db->get()->result();
	}
	public function specialist_table($id)
	{
		
		$this->db->where("specialist.hosp_id",$id);
		$this->db->select("specialist.sp_name,specialist.sp_id,specialist.hosp_id");
		$this->db->from("specialist");
		//$this->db->join("hospitals","specialist.hosp_id=hospitals.hosp_id");
		return $this->db->get()->result();
	}
	public function dropdawn_areaname()
	{
		$this->db->select("area.area_name,area.area_id");
		$this->db->from("area");
		return $this->db->get()->result();
	}
	public function doctors_table($id)
	{
		$this->db->where("doctors.sp_id",$id);
		$this->db->select("doctors.doc_name,doctors.doc_deg,doctors.doc_time");
		$this->db->from("doctors");
		return $this->db->get()->result();
	}
	public function Insert($table,$data)
	{
            
		if($this->db->insert($table,$data))
		{
			return true;
		}
		return false;
	}
	public function view_data_multiple_table($table1,$table2,$select,$relation)
	{
		$this->db->select($select);
		$this->db->from($table1);
		$this->db->join($table2,$relation);
		return $this->db->get()->result();
	}
	public function view_data($table)
	{
		$this->db->select("*");
		$this->db->from($table);
		return $this->db->get()->result();
	}
        public function view_data2()
	{
		$this->db->select("hosp_id,hosp_name");
		$this->db->from("hospitals");
		return $this->db->get()->result();
	}
	public function edit_data($table,$id)
	{
		$this->db->where($id);
		$this->db->select("*");
		$this->db->from($table);
		return $this->db->get()->result();
	}
	public function update_data($table,$data,$id)
	{
		$this->db->where($id);
		if($this->db->update($table,$data))
		{
			return true;
		}
		return false;
	}
	public function delete_data($table,$id)
	{
		if($this->db->delete($table,$id))
		{
			return true;
		}
		return false;
	}
	function hospital_search($hosp_name){
		$this->db->like("hospitals.hosp_name",$hosp_name);
		$this->db->select("hospitals.hosp_id, hospitals.hosp_name,hospitals.hosp_add,hospitals.hosp_con,hospitals.Email,hospitals.Web");
		$this->db->from("hospitals");
		return $this->db->get()->result();
                
                
	}
        public function doctors_info()
        {
            $this->db->select('*');
            $this->db->from('doctors d'); 
            $this->db->join('specialist s', 's.sp_id=d.sp_id');
            $this->db->join('hospitals h', 'h.hosp_id=s.hosp_id');
            return $this->db->get()->result();
            
           
        }
}

?>