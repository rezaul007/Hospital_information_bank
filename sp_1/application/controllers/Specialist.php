<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Specialist extends CI_Controller {
public function spename(){
	    $data = array();
	    $id=$this->uri->segment(3);
		$data['pg_title'] = "Hospital information bank";
		$data['model'] = $this->mm->specialist_table($id);
                $data['hospitalname'] = $this->mm->view_data2();
                if($data['model'] != NULL){
		$data['content'] = $this->load->view("specialistinfo", $data, true);
                }
                else{
                    $data['content'] = $this->load->view("sorry", $data, true);
                }
		$this->load->view('master', $data);
	}
}
	
?>